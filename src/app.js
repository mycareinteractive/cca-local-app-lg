import $ from 'jquery';
import './helpers/dotimeout';
import TvScreen from './service/tvscreen';
import KeyCodes from './device/keycodes';

import './css/app.css';

// If test on PC use default desktop from Pelican
if (!!window.chrome) {
    var Desktop = require('./device/desktop').default;
    window.PelicanDevice = new Desktop();
    console.log("Platform: Desktop Chrome");
}
else {
    console.log("Platform: Procentric -- LOADING!");
    var Procentric = require('./device/procentric').default;
    console.log("Platform: Procentric -- LOADING !!!!");
    window.PelicanDevice = new Procentric();
    console.log("Platform: Procentric");
}

// Instantiate App and components
const App = window.App = {
    data: {},

    config: {},

    device: PelicanDevice,

    tv: new TvScreen(),

    cheats: {
        reload: [KeyCodes.Menu, KeyCodes.Num2, KeyCodes.Num5, KeyCodes.Num8, KeyCodes.Num0]
    },

    start: function () {
        var self = this;
        console.log('Starting the APP');

        self.device.bootstrap();
        self.tv.initialize();

        // load production config first
        $.getJSON('./config.prod.json')
            .done(function (data) {
                console.log('Configuration loaded successfully from "config.prod.json"');
                self.config = data;
            })
            .fail(function () {
                console.log('Failed to load configuration from "config.prod.json"!');
            })
            .always(function () {
                // always try to load override config
                $.getJSON('./config.json')
                    .done(function (data) {
                        console.log('Override configuration loaded successfully from "config.json"');
                        $.extend(self.config, data);
                    })
                    .always(function () {
                        console.log('Config ready');
                        // check server
                        self.checkServer();

                        // if nothing happens play TV in 10 seconds
                        $.doTimeout('play tv timer', 10000, function () {
                            self.playTv();
                        });

                        // also check server every minute
                        $.doTimeout('check server timer', 60000, function () {
                            self.checkServer();
                            return true;
                        });
                    });
            });
    },

    stop: function () {
        this.tv.onScreenHide();
        $.doTimeout('play tv timer');
        $.doTimeout('check server timer');
    },

    checkServer: function () {
        var self = this;
        console.log('Checking server status on ' + self.config.server + ' ...');
        var d1 = $.ajax({
            url: self.config.server + '/startup/',
            //dataType: 'jsonp',
            cache: false,
            timeout: 5000,
        });

        var d2 = $.ajax({
            url: self.config.server + '/api/v1/time',
            cache: false,
            timeout: 5000
        });

        $.when(d1, d2).done(function () {
            console.log('server is ONLINE!');
            location.href = self.config.server + '/startup/';
        }).fail(function () {
            console.log('server is OFFLINE!');
        });
    },

    playTv: function () {
        this.tv.onScreenShow();
        this.tv.playStartChannel();
    },

    triggerKey: function (e, key) {
        e.preventDefault();
        console.log('processing key ' + key);
        this.checkCheatCodes(key);
        this.tv.onKey(e, key);
    },

    checkCheatCodes: function (key) {
        if (this.isCheat(key, this.cheats.reload)) {
            if (PelicanDevice.reload)
                PelicanDevice.reload();
        }
    },

    isCheat: function (key, cheatKeys) {
        var idx = cheatKeys.match || 0;
        var result = false;

        if (!cheatKeys || !cheatKeys.length || !key)
            return false;

        if (idx < 0 || idx >= cheatKeys.length) {
            idx = 0;
            result = false;
        }
        else if (cheatKeys[idx] === key) {
            idx++;   // hit, increase match index

            if (idx >= cheatKeys.length) {    // bingo, all hit
                console.log('^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ');
                console.log('^-^ cheat code detected - "' + cheatKeys.toString() + '" ^-^');
                console.log('^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ^-^ ');
                idx = 0;
                result = true;
            }
            else {
                result = false;
            }
        }
        else {
            idx = 0; // missed, reset index
            result = false;
        }

        cheatKeys.match = idx;
        return result;
    }
};

window.onunload = function () {
    console.log('local app stopping');
    App.stop();
};

window.onload = function () {
    var wait = Math.floor(Math.random() * (30000 - 5000) + 5000);
    console.log('Waiting for ' + wait + 'ms before starting...');
    $.doTimeout(wait, function () {
        console.log('local app starting');
        App.start();
    });
};

export default App;

